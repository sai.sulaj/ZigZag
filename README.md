# CogniSynth ZigZag

This is CogniSynth's custom in-house implementation of the ZigZag indicator built on the Node JS platform.

### Prerequisites

Before running anything, install all dependencies

```
npm i --save
```

## Running the tests

Testing is implemented using mocha, run the npm test command to execute them.

```
npm run test
```

Currently only critical utilities have testing implemented.

## Built With

* [CCXT](https://github.com/ccxt/ccxt) - Data source for testing.
* [Chai](https://www.npmjs.com/package/chai) - Testing utility.
* [MathJS](http://mathjs.org/)
* [Dotenv](https://www.npmjs.com/package/dotenv)
* [Getenv](https://www.npmjs.com/package/getenv)
* [csv-parse](https://www.npmjs.com/package/csv-parse)
* [csv-stringify](https://www.npmjs.com/package/csv-stringify)
* [fs](https://www.npmjs.com/package/fs)

## Authors

* **Saimir Sulaj** - [sai.sulaj](https://gitlab.com/sai.sulaj)

## Acknowledgments

* Coffee
