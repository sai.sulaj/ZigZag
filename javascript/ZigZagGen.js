'use strict';
/**
 * Author: Saimir Sulaj.
 * Date: August 15, 2018.
 */

// ----------- DEPENDENCIES ----------- //

require('dotenv').config();

const Getenv   = require('getenv');
const MathJS   = require('mathjs');
const CCXT     = require('ccxt');
const csvS     = require('csv-stringify');
const Fs       = require('fs');

// ----------- LOCAL DEPENDENCIES ----------- //

const Errors   = require('./Errors');
const Utils    = require('./Utils');

// ----------- CLASS BLUEPRINT ----------- //

/**
 * Generates zigzag lines on timeseries data.
 *
 * @public
 */
class ZigZagGen {
  /**
   * Initializes class instance.
   *
   * @type {function(int=): ZigZagGen}
   *
   * @public
   * @constructor
   *
   * @param {int} _Depth
   */
  constructor(_Depth=Getenv.int('ZIGZAG_DEPTH', 15)) {
    this.direction = 1;
    this.zzH       = [];
    this.zzL       = [];
    this.last      = 0;
    this.depth     = _Depth;
    this.range     = null;

    this.lastTimestamp = null;
  }

  /**
   * Fetches a copy of all highs in the timeframe considered.
   *
   * @type {function(): Array<float>}
   *
   * @public
   *
   * @return {Array<float>}
   */
  getHighs() {
    return this.zzH.slice(0);
  }

  /**
   * Fetches a copy of all lows in the timeframe considered.
   *
   * @type {function(): Array<float>}
   *
   * @public
   *
   * @return {Array<float>}
   */
  getLows() {
    return this.zzL.slice(0);
  }

  /**
   * Fetches a copy of all highs and all
   * lows merged into an array in the timeframe
   * considered.
   *
   * @type {function(): Array<float>}
   *
   * @private
   *
   * @return {Array<float>}
   */
  mergeHighLows() {
    let highs  = this.getHighs();
    let lows   = this.getLows();
    let output = [];

    loopOne:
    for (let i = 0; i < highs.length; i++) {
      if (highs[i].extrema !== 0 && lows[i].extrema !== 0) {
        output.push({extrema: 0});
        continue loopOne;
      }

      output.push({
        extrema: Math.max(highs[i].extrema, lows[i].extrema),
        timestamp: highs[i].timestamp,
      });
    }

    return output;
  }

  /**
   * Finds support and resistance lines
   * by comparing all extrema and finding
   * values within a common range of total
   * data range * ZIGZAG_RANGE_BOUND_PERCENT.
   * Potential candidates are groups of extrema
   * of length 4 or more in a common range. The
   * 25 potential zones with the lowest standard
   * deviation of validation extrema are then selected,
   * and sorted by the distance between the means
   * of other potentials and returned.
   *
   * @type {function(): Array<JSON>}
   *
   * @public
   *
   * @return {Array<JSON>}
   */
  findSupportResistance() {
    let highsLows = this.mergeHighLows().map((item) => {
      // Set all empty entries to null.
      return item.extrema === 0 ? null : item;
    });

    let bounds = this.range * Getenv.float('ZIGZAG_RANGE_BOUND_PERCENT', 0.03);
    let potentials = [];

    let a = 0;
    let b = 0;
    let tmp = [];
    loopA:
    while (a++ < highsLows.length) {
      if (highsLows[a] === null || highsLows[a] === undefined) continue loopA;

      b = a;
      loopB:
      while (b++ < highsLows.length) {
        if (highsLows[b] === null || highsLows[b] === undefined) continue loopB;

        if (Math.abs(highsLows[a].extrema - highsLows[b].extrema) <= bounds) {
          tmp.push(highsLows[b]);
        }
      }

      /**
       * If support/resistance line is validated
       * 3 or more times, consider it a viable candidate.
       */
      if (tmp.length > 3) {
        potentials.push(tmp);
      }

      tmp = [];
    }

    /**
     * Sort potential high/low values
     * by standard deviation of validation
     * points, lowest to highest.
     */
    potentials = potentials.slice(1).sort((a, b) => {
      let stdOne = MathJS.std(a.map((item) => {
        return item.extrema;
      }));
      let stdTwo = MathJS.std(b.map((item) => {
        return item.extrema;
      }));
      return stdOne - stdTwo;
    });

    // Select top 25 candidates.
    potentials = potentials.slice(0, 25);

    let potentialMeans = potentials.map((entry, i) => {
      let extremas = entry.map((entry) => {
        return entry.extrema;
      });
      return {
        index: i,
        mean: MathJS.mean(extremas),
      };
    });

    /**
     * Calculate distance between means
     * of candidates.
     */
    let summedDist = [];
    sumLoopA:
    for (let a = 0; a < potentialMeans.length; a++) {
      let sum = 0;

      sumLoopB:
      for (let b = 0; b < potentialMeans.length; b++) {
        if (a === b) continue sumLoopB;

        sum += Math.abs(potentialMeans[a].mean - potentialMeans[b].mean);
      }

      summedDist.push({
        index: a,
        sum: sum,
      });
    }
    // Sort distances between means.
    summedDist = summedDist.sort((a, b) => {
      return b.sum - a.sum;
    });

    /**
     * Sort candidates by distance between
     * their respective means, greatest to
     * least.
     */
    let ordered = summedDist.map((entry) => {
      return entry.index;
    }).reduce((p, c) => {
      p.push(potentials[c]);
      return p;
    }, []);

    return ordered;
  }

  /**
   * Calculates fibonacci retracement lines
   * from stored extrema in the specified
   * range.
   *
   * Returned array items are in the form:
   * {
   *   line: <float>,
   *   timestamp: <int>
   * }
   *
   * @type {function(int, int): Array<JSON>}
   *
   * @public
   *
   * @throws {Errors.InvalidParamsError}
   *
   * @param {int} _FromI
   * @param {int} _ToI
   * @return {Array<JSON>}
   */
  getFibRetracements(_FromI, _ToI) {
    let highsLows = this.mergeHighLows();
    let outputs = [];

    let low = highsLows[_FromI];
    let high = highsLows[_ToI];

    if (low.extrema === 0 || high.extrema == 0) {
      throw new Errors.InvalidParamsError(`Fib params invalid, Not extrema -> low: ${low}, high: ${high}`);
    } else if (low.extrema > high.extrema) {
      throw new Errors.InvalidParamsError(`Fib params invalid, low above high -> low: ${low}, high: ${high}`);
    }

    for (let i = 0; i < Getenv.array('FIBONACCI_RETRACEMENTS', 'float').length; i++) {
      outputs.push({
        line: low.extrema + (high.extrema - low.extrema) * Getenv.array('FIBONACCI_RETRACEMENTS', 'float')[i],
        timestamp: low.timestamp,
      });
    }

    return outputs;
  }

  /**
   * Returns list of ranges in
   * stored data with bullish or
   * bearish trends.
   *
   * Returned array items are in the form:
   * {
   *   trend: <'bullish' | 'bearish'>,
   *   from: <int>,
   *   to: <int>
   * }
   *
   * @type {function(): Array<JSON>}
   *
   * @public
   *
   * @return {Array<JSON>}
   */
  getTrendDomains() {
    let highsLows = this.mergeHighLows();
    let output = [];

    let lastExtremaI = null;
    for (let i = 0; i < highsLows.length; i++) {
      if (highsLows[i].extrema !== 0) {
        if (lastExtremaI === null) lastExtremaI = i;
        else {
          let trend = highsLows[i].extrema - highsLows[lastExtremaI].extrema > 0
            ? 'bullish' : 'bearish';
          output.push({
            trend: trend,
            from: lastExtremaI,
            to: i,
          });
          lastExtremaI = i;
        }
      }
    }

    return output;
  }

  /**
   * Stores high/low data in object.
   *
   * @type {function(int, Array<int>, Array<float>, Array<float>, Array<float>, Array<float>): int}
   *
   * @public
   *
   * @param {int} _Calculated
   * @param {Array<int>} _TimeA
   * @param {Array<float>} _OpenA
   * @param {Array<float>} _HighA
   * @param {Array<float>} _LowA
   * @param {Array<float>} _CloseA
   * @return {int}
   */
  gen(_Calculated, _TimeA, _OpenA, _HighA, _LowA, _CloseA) {
    if (_Calculated == 0) {
      this.last = 0;

      this.range = Math.max(..._HighA) - Math.min(..._LowA);
      this.depth = this.range * Getenv.float('ZIGZAG_RANGE_DELTA', 0.10);
    }

    for (let i = _Calculated > 0 ? _Calculated - 1 : 0; i < _TimeA.length; i++) {
      let set = false;

      this.zzL[i] = {extrema: 0, timestamp: _TimeA[i]};
      this.zzH[i] = {extrema: 0, timestamp: _TimeA[i]};

      if (this.direction > 0) {
        if (_HighA[i] > this.zzH[this.last].extrema) {
          this.zzH[this.last] = {extrema: 0, timestamp: _TimeA[i]};
          this.zzH[i] = {extrema: _HighA[i], timestamp: _TimeA[i]};

          if (_LowA[i] < _HighA[this.last] - this.depth) {
            if (_OpenA[i] < _CloseA[i]) {
              this.zzH[this.last] = {extrema: _HighA[this.last], timestamp: _TimeA[this.last]};
            } else {
              this.direction = -1;
            }

            this.zzL[i] = {extrema: _LowA[i], timestamp: _TimeA[i]};
          }

          this.last = i;
          set = true;
        }

        if (_LowA[i] < this.zzH[this.last].extrema - this.depth
            && (!set || _OpenA[i] > _CloseA[i])) {
          this.zzL[i] = {extrema: _LowA[i], timestamp: _TimeA[i]};

          if (_HighA[i] > this.zzL[i].extrema + this.depth
              && _OpenA[i] < _CloseA[i]) {
            this.zzH[i] = {extrema: _HighA[i], timestamp: _TimeA[i]};
          } else {
            this.direction = -1;
          }

          this.last = i;
        }
      } else {
        if (_LowA[i] < this.zzL[this.last].extrema) {
          this.zzL[this.last] = {extrema: 0, timestamp: _TimeA[i]};
          this.zzL[i] = {extrema: _LowA[i], timestamp: _TimeA[i]};

          if (_HighA[i] > _LowA[this.last] + this.depth) {
            if (_OpenA[i] > _CloseA[i]) {
              this.zzL[this.last] = {extrema: _LowA[this.last], timestamp: _TimeA[this.last]};
            } else {
              this.direction = 1;
            }

            this.zzH[i] = {extrema: _HighA[i], timestamp: _TimeA[i]};
          }

          this.last = i;
          set = true;
        }

        if (_HighA[i] > this.zzL[this.last].extrema + this.depth
            && (!set || _OpenA[i] < _CloseA[i])) {
          this.zzH[i] = {extrema: _HighA[i], timestamp: _TimeA[i]};

          if (_LowA[i] < this.zzH[i].extrema - this.depth
              && _OpenA[i] > _CloseA[i]) {
            this.zzL[i] = {extrema: _LowA[i], timestamp: _TimeA[i]};
          } else {
            this.direction = 1;
          }

          this.last = i;
        }
      }
    }

    this.zzH[_TimeA.length - 1] = 0;
    this.zzL[_TimeA.length - 1] = 0;
    return _TimeA.length;
  }

  /**
   * Takes CCXT data and adds an extrema column.
   *
   * @type {function(_Array<Array<*>>): _Array<Array<number>>}
   *
   * @public
   *
   * @param {Array<Array<*>>} _Data
   * @return {Array<Array<number>>}
   */
  mergeCCXTExtrema(_Data) {
    let index = _Data.findIndex((item) => {
      return item[0] === this.lastTimestamp;
    });
    index = Math.max(index, 0);

    let pData = Utils.parseCCXTTimeseries(_Data);

    this.gen(index, pData.time, pData.open, pData.high, pData.low, pData.close);

    let highLows = this.mergeHighLows();
    _Data = _Data.map((item, i) => {
      item.push(highLows[i].extrema);
      return item;
    });
    return _Data;
  }

  /**
   * Downloads recent OHLCV data from CCXT Poloniex and returns CSV file.
   *
   * @type {function(string=)}
   *
   * @public
   * @static
   * @async
   *
   * @param {string} _FileName
   * @param {Date} _StartDate
   * @param {string} _TimeFrame
   */
  async testLive(_FileName, _StartDate, _TimeFrame) {
    let poloniex = new CCXT.poloniex();
    let data = await poloniex.fetchOHLCV('DASH/USDT', _TimeFrame, _StartDate.getTime());
    data = this.mergeCCXTExtrema(data);
    console.log(this.findSupportResistance());

    csvS(data.map((item) => {
      item[0] *= 0.001;
      item[6] = item[6] === 0 ? '' : item[6];
      return item;
    }), function(err, output) {
      Fs.writeFileSync(_FileName, 'Time,Open,High,Low,Close,Vol,Extrema,\n' + output);
    });
  }
}

module.exports = ZigZagGen;
