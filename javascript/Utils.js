/**
 * Author: Saimir Sulaj.
 * Date: August 15, 2018.
 */

// ---------- DEPENDENCIES ---------- //

const csvP   = require('csv-parse/lib/sync');

const Fs     = require('fs');

// ---------- LOCAL DEPENDENCIES ---------- //

const Errors = require('./Errors');

// ---------- CLASS BLUEPRINT ---------- //

/**
 * Utility class containing random useful functions.
 *
 * @public
 * @static
 */
class Utils {
  /**
   * Adapts CCXT timeseries OHLCV data to
   * proper zigzag format.
   *
   * Return JSON format is:
   * {
   *   time: Array<float>,
   *   open: Array<float>,
   *   high: Array<float>,
   *   low: Array<float>,
   *   close: Array<float>,
   * }
   *
   * @type {function(Array<JSON>): JSON}
   *
   * @public
   * @static
   *
   * @param {Array<JSON>} _CCXTData
   * @return {JSON}
   */
  static parseCCXTTimeseries(_CCXTData) {
    let time = [];
    let open = [];
    let high = [];
    let low = [];
    let close = [];

    for (let i = 0; i < _CCXTData.length; i++) {
      time.push(_CCXTData[i][0]);
      open.push(parseFloat(_CCXTData[i][1]));
      high.push(parseFloat(_CCXTData[i][2]));
      low.push(parseFloat(_CCXTData[i][3]));
      close.push(parseFloat(_CCXTData[i][4]));
    }

    return {
      time: time,
      open: open,
      high: high,
      low: low,
      close: close,
    };
  }

  /**
   * Converts CSV OHLCV data with date string
   * as first parameter to correct ZigZag
   * format.
   *
   * @type {function(Array<Array<*>>): Array<Array<int>>}
   *
   * @public
   * @static
   *
   * @param {Array<Array<*>>} _Data
   * @return {Array<Array<int>>}
   */
  static parseCSV(_Data) {
    let dates = _Data.map((entry) => {
      return this.parseDateString(entry[0]);
    });
    _Data = _Data.map((entry, i) => {
      entry[0] = dates[i];
      return entry;
    });

    return this.parseCCXTTimeseries(_Data);
  }

  /**
   * Converts date string of format yyyy-mm-dd
   * to unix timestamp.
   *
   * @type {function(string): int}
   *
   * @private
   * @static
   *
   * @throws {Errors.InvalidParamsError}
   *
   * @param {string} _Str
   * @return {int}
   */
  static parseDateString(_Str) {
    if ((_Str.match(/-/g) || []).length !== 2) throw new Errors.InvalidParamsError(`Date string does not contain two dashes: ${_Str}`);
    let strArr = _Str.split('-');
    return (new Date(strArr[0], strArr[1] - 1, strArr[2])).getTime();
  }

  /**
   * Returns parsed test dataset.
   *
   * @type {function(): Array<Array<float>>}
   *
   * @public
   * @static
   *
   * @return {Array<Array<float>>}
   */
  static getTestDataset() {
    return this.parseCSV(csvP(Fs.readFileSync(__dirname + '/../test/aapl_ohlcv_1999-2017.csv')));
  }
}

module.exports = Utils;
