'use strict';
/**
 * Error thrown when invalid parameters
 * are passed to a function.
 */
class InvalidParamsError extends Error {
  /**
   * Initializes class instance.
   *
   * @type {function(...*)}
   * @extends {Error}
   *
   * @public
   * @constructor
   *
   * @param  {...*} args
   */
  constructor(...args) {
    super(...args);
    Error.captureStackTrace(this, InvalidParamsError);
  }
}

module.exports = {
  InvalidParamsError: InvalidParamsError,
};
