// ----------- DEPENDENCIES ----------- //

const Fs      = require('fs');
const csvP    = require('csv-parse/lib/sync');
const Assert  = require('chai').assert;

// ----------- LOCAL DEPENDENCIES ----------- //

const ZZGen   = require('../javascript/ZigZagGen');
const Utils   = require('../javascript/Utils');
const Errors  = require('../javascript/Errors');

// ----------- CONSTANTS & GLOBAL VARS ----------- //

const zz = new ZZGen();
const testData = csvP(Fs.readFileSync('./test/aapl_ohlcv_1999-2017.csv'));

// ----------- MAIN ----------- //

describe('Beginning testing', function() {
  let utilsPassed = true;

  describe('Utils class', function() {
    describe(':Utils#parseDateString function', function() {
      it('Should throw error if wrong number of dashes', function() {
        Assert.throws(() => {
          Utils.parseDateString('2094-24-34-');
        }, Errors.InvalidParamsError);
      });

      it('Should return timestamp', function() {
        Assert.typeOf(Utils.parseDateString('2016-04-02'), 'number');
      });

      afterEach(function() {
        if (this.currentTest.state === 'failed') { // eslint-disable-line
          utilsPassed = false;
        }
      });
    });

    if (utilsPassed) {
      describe(':Utils#parseCSV', function() {
        let parsedTestData = Utils.parseCSV(testData);

        it('Should have equal lengths of input and output time data', function() {
          Assert.equal(parsedTestData.time.length, testData.length);
        });

        it('Should have equal lengths of input and output open data', function() {
          Assert.equal(parsedTestData.open.length, testData.length);
        });

        it('Should have equal lengths of input and output high data', function() {
          Assert.equal(parsedTestData.high.length, testData.length);
        });

        it('Should have equal lengths of input and output low data', function() {
          Assert.equal(parsedTestData.low.length, testData.length);
        });

        it('Should have equal lengths of input and output close data', function() {
          Assert.equal(parsedTestData.close.length, testData.length);
        });

        it('Should not contain any NaN values', function() {
          for (let r = 0; r < parsedTestData.time.length; r++) {
            Assert.isNotNaN(parsedTestData.open[r]);
            Assert.isNotNaN(parsedTestData.high[r]);
            Assert.isNotNaN(parsedTestData.low[r]);
            Assert.isNotNaN(parsedTestData.close[r]);
          }
        });
      });
    }
  });
});
